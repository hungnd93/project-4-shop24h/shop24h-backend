const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");

router.post("/products", productController.createProduct);
router.get("/products", productController.getAllProduct);
router.get("/products/limit",productController.getProductLimit);
router.get("/products/start/limit", productController.getProductStartLimit);
router.get("/products/type", productController.getProductByType);
router.get("/products/:productId", productController.getProductById);
module.exports = router;