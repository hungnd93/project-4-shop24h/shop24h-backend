const { default: mongoose } = require("mongoose");
const productModel = require("../model/productModel");
//function tạo product mới
const createProduct = (req, res) => {
    const body = req.body;

    if (!body.Id) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Id Product không hợp lệ"
        })
    }
    if (!body.Name) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Name Product không hợp lệ"
        })
    }
    if (!body.Type) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Type Product không hợp lệ"
        })
    }
    if (!body.ImageUrl) {
        return res.status(400).json({
            status: "Bad Request",
            message: "ImageUrl Product không hợp lệ"
        })
    }
    if (!body.BuyPrice) {
        return res.status(400).json({
            status: "Bad Request",
            message: "BuyPrice Product không hợp lệ"
        })
    }
    if (!body.PromotionPrice) {
        return res.status(400).json({
            status: "Bad Request",
            message: "PromotionPrice Product không hợp lệ"
        })
    }

    const newProduct = {
        Id: body.Id,
        Name: body.Name,
        Type: body.Type,
        ImageUrl: body.ImageUrl,
        BuyPrice: body.BuyPrice,
        PromotionPrice: body.PromotionPrice,
        Description: body.Description
    }
    productModel.create(newProduct, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(201).json({
            message: "Create new product successfully",
            Product: data
        })
    })
}
//tạo function get product
const getAllProduct = (req, res) => {
    productModel
        .find()
        .exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return res.status(200).json({
                status: "Get all Products successfully",
                data: data
            })
        })
}
// get product limit
const getProductLimit = (req, res) => {

    const limit = req.query.limit;

    if (limit) {
        productModel.find()
            .limit(limit)
            .exec((err, data) => {
                if (err) {
                    return res.status(500).json({
                        status: "Error 500: Internal server error",
                        message: err.message
                    })
                }
                else {
                    return res.status(200).json({
                        status: "Get limited product successfully",
                        data: data
                    })
                }
            })
    }
    else {
        productModel.find((error, data) => {
            if (error) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return res.status(200).json({
                status: "Get all products successfully!",
                data: data
            })
        })
    }

}
// phân trang
const getProductStartLimit = (req, res) => {

    const start = req.query.start;
    const limit = req.query.limit;

    if (start) {
        productModel.find()
            .skip(start)
            .limit(limit)
            .exec((err, data) => {
                if (err) {
                    return res.status(500).json({
                        status: "Error 500: Internal server error",
                        message: err.message
                    })
                }
                else {
                    return res.status(200).json({
                        status: "Get limited product successfully",
                        data: data
                    })
                }
            })
    }
    else {
        productModel.find((error, data) => {
            if (error) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return res.status(200).json({
                status: "Get all products successfully!",
                data: data
            })
        })
    }

}

const getProductByType = (req, res)=> {
    const TypeProduct =  req.query.TypeProduct;
    if(TypeProduct){
    productModel.find({Type: TypeProduct}, (errorFindProduct, TypeProductExist) => {
        if(errorFindProduct){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: errorFindProduct.message
            })
        } else{
            if(!TypeProduct){ 
                return res.status(400).json({
                    status:" Type product is not valid",
                    data: []
                })
            } else {
                productModel.find({Type: TypeProduct} ,(error, data) =>{
                    if(error){
                        return res.status(500).json({
                            status:" Internal server error",
                            message: error.message
                        })
                    }
                    return res.status(200).json({
                        status:`Get product by ${TypeProduct} successfully`,
                        Product: data
                    })
                })
            }
        }
    })
    }
}
//get product by id
const getProductById = (req,res) =>{
    const productId = req.params.productId
    if(!mongoose.Types.ObjectId.isValid(productId)){
        return res.status(400).json({
            status:"Bad request",
            message:"product id is not valid"
        })
    }
    productModel.findById(productId, (error, data) =>{
        if(error){
            return res.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status:`Get product successfully`,
            data: data
        })
    })
}
 

module.exports = {
    createProduct,
    getAllProduct,
    getProductLimit,
    getProductStartLimit,
    getProductByType,
    getProductById
};
