const express = require("express");
const mongoose = require("mongoose");
const cors = require('cors')
const app = express();
const port  = 8000;
app.use(express.json());
app.use(cors());

mongoose.connect("mongodb://127.0.0.1:27017/project_shop24h",(error)=>{
    if(error) throw error;
    console.log("Connect to database Shop24h successfully")
}) 
const productRouter = require("./app/routes/productRouter");
app.use("/api", productRouter);

app.listen(port, ()=>{
    console.log(`App is running on port ${port}`)
})