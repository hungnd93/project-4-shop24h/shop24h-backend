const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const productSchema = new  Schema ({
    Id: {
        type: Number,
        required: true
    },
    Name: {
        type: String,
        required: true
    },
    Type: {
        type: String,
        required: true
    }, 
    ImageUrl: {
        type: String,
        required: true
    },
    BuyPrice: {
        type: Number,
        required: true
    }, 
    PromotionPrice: {
        type: Number,
        required: true
    },
    Description: {
        type: String,
        required: true
    }

});

module.exports = mongoose.model("Products", productSchema);